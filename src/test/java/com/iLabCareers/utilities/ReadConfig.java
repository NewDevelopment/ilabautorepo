/**
 * 
 */
package com.iLabCareers.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Onke Ngcatsha
 *
 */
public class ReadConfig {

	Properties pro;

	public ReadConfig() {

		File src = new File("./Configurations/Config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getApplicationBaseUrl() {

		String url = pro.getProperty("BaseUrl");
		return url;
	}

	public String getName() {

		String applicantName = pro.getProperty("name");
		return applicantName;
	}

	public String getEmail() {

		String applicantEmail = pro.getProperty("email");
		return applicantEmail;
	}

	public String getInputNumber() {

		String inputNumber = pro.getProperty("input");
		return inputNumber;
	}

	public String getExpetedMessage() {

		String expectedmsg = pro.getProperty("expectedMessage");
		return expectedmsg;
	}

	public String getFireFoxPath() {

		String ffpath = pro.getProperty("fireFoxPath");
		return ffpath;
	}
	
	public String getChromePath() {
		String chromePath = pro.getProperty("chrome");
		return chromePath;
	}

}

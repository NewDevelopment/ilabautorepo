/**
 * 
 */
package com.iLabCareers.Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Onke Ngcatsha
 *
 */
public class HomePage {

	WebDriver ldriver;

	public HomePage(WebDriver rdriver) {

		this.ldriver = rdriver;

		PageFactory.initElements(rdriver, this);
	}

	@FindBy(xpath = "//*[@id=\"menu-item-1373\"]/a")
	WebElement linkBtnCareers;

	public void clickOnCareers() {

		linkBtnCareers.click();
	}
}

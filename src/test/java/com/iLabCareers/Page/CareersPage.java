/**
 * 
 */
package com.iLabCareers.Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



/**
 * @author Onke Ngcatsha
 *
 *
 *         This page processes the online Application.
 */
public class CareersPage {

	WebDriver ldriver;
	public CareersPage(WebDriver rdriver) {

		this.ldriver = rdriver;

		PageFactory.initElements(rdriver, this);
	}

	@FindBy(linkText = "South Africa")
	WebElement linkTextSA;

	@FindBy(xpath = "/html/body/section/div[2]/div/div/div/div[3]/div[2]/div/div/div/div/div/div[1]/div[1]/div[2]/div[1]/a")
	WebElement firstJobLinkText;

	@FindBy(xpath = "//*[@id=\"wpjb-scroll\"]/div[1]/a")
	WebElement applyLinkText;

	@FindBy(id = "applicant_name")
	WebElement application;

	@FindBy(id = "email")
	WebElement emailAddress;

	@FindBy(name = "phone")
	WebElement txtTelephoneSA;

	@FindBy(id = "wpjb_submit")
	WebElement btnSubmit;
	
	@FindBy(xpath = "//*[@id=\"wpjb-apply-form\"]/fieldset[1]/div[5]/div/ul/li")
	WebElement actualMessage;

	public void clickSouthAfrica() {
		linkTextSA.click();
		
	}
	

	public void clickFirstJob() {
		firstJobLinkText.click();
	}

	public void clickApplyOnline() {
		applyLinkText.click();
	}

	public void enterApplicantName(String name) {
		application.sendKeys(name);
	}

	public void enterEmail(String email) {

		emailAddress.sendKeys(email);
	}

	public void enterPhoneNum(String phoneNumber) {

		txtTelephoneSA.sendKeys(phoneNumber);

	}

	public void clickBtnSubmit() {
		btnSubmit.click();
	}


}

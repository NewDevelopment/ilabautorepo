/**
 * 
 */
package com.iLabCareers.TestCase;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.iLabCareers.Page.CareersPage;
import com.iLabCareers.Page.HomePage;


/**
 * 
 * This Class verify the online job application functionality.
 *
 */
public class TC_ApplyOnlineTest_001 extends BaseClass {
	
	@Test
	public void VerifyOnlineApplication() {
		
		try {
			driver.get(BaseUrl);
			HomePage hp = new HomePage(driver);
			hp.clickOnCareers();
			logger.info("Click careers");
			
			CareersPage cp = new CareersPage(driver);
			cp.clickSouthAfrica();
			logger.info("Click South Africa");
			
			cp.clickFirstJob();
			logger.info("Click first job");
			
			cp.clickApplyOnline();
			logger.info("Click to submit online application");
			
			cp.enterApplicantName(name);
			logger.info("Entered Name ");
			
			cp.enterEmail(email);
			logger.info("Entered Email");
			
			cp.enterPhoneNum(phoneNumber);
			logger.info("Entered Phone number");
			
			cp.clickBtnSubmit();
			logger.info("Submit application");
			
			
			String actualMessage = driver.findElement(By.xpath("//*[@id=\"wpjb-apply-form\"]/")).getText();
			if (actualMessage.contentEquals(actualMessage))
			{
				Assert.assertTrue(true);
				logger.info("Test Paased");
				
			}
			else {
				Assert.assertTrue(false);
				logger.info("Test Failed");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}

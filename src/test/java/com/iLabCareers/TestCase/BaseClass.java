/**
 * 
 */
package com.iLabCareers.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.iLabCareers.utilities.ReadConfig;

/**
 * @author Onke Ngcatsha
 * 
 * 
 *         This class will store all the common resources
 *
 */
public class BaseClass {

	public static WebDriver driver;
	public static Logger logger;

	ReadConfig readconfig = new ReadConfig();

	public String BaseUrl = readconfig.getApplicationBaseUrl();
	public String name = readconfig.getName();
	public String email = readconfig.getEmail();
	String input = readconfig.getInputNumber();
	String phoneNumber = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
	String expectedMessage = readconfig.getExpetedMessage();

	@Parameters("browser")
	@BeforeTest
	public void setUp(String br) {

		logger = Logger.getLogger("iLabAutomation");
		PropertyConfigurator.configure("log4j.properties");

		if (br.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + readconfig.getFireFoxPath());
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		}
		else if (br.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + readconfig.getChromePath());
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			
		}

	}

	@AfterTest
	public void tearDown() {
		driver.quit();

	}

}
